import React, { Component } from 'react'
import {
  Typography,
  Grid,
  Paper,
  AppBar,
  Tabs,
  Tab,
  Toolbar,
  StepButton
} from '@material-ui/core'
import AceEditor from 'react-ace'
import 'ace-builds/src-noconflict/mode-json'
import 'ace-builds/src-noconflict/mode-html'
import 'ace-builds/src-noconflict/theme-monokai'
import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepContent from '@material-ui/core/StepContent'
import uuid from 'uuid'
import config from './config'

import themeSample from './samples/theme.example.json'

export default class App extends Component {
  constructor (props) {
    super(props)
    console.info(`Pipeline id: ${process.env.REACT_APP_CI_PIPELINE_ID}`)
    this.state = {
      tab: 'html',
      tab2: 'preview',
      tab3: 'widget',
      tabWidgetMode: 'blank',
      appId: uuid.v4(),
      sourceDomain: '',
      themeData: JSON.stringify(themeSample, null, 4),
      activeStep: 'integrate'
    }
  }

  handleValidate = data => {
    try {
      JSON.parse(data)
      return true
    } catch (error) {
      console.error(error)
      return false
    }
  }

  handleStep (index) {
    this.setState({ activeStep: index })
  }

  render () {
    const { tab, tab2, tab3, tabWidgetMode, activeStep } = this.state
    const externalParameters = `?appId=${encodeURIComponent(
      this.state.appId
    )}&sourceDomain=${encodeURIComponent(this.state.sourceDomain)}`
    const previewParameters = `?appId=1eaeddd1-6de3-450d-ad37-24c130f273ad&sourceDomain=${window.location.host}`

    let htmlParameters = ''
    let jsParameters = ''
    let jsVars = ''

    if (tabWidgetMode === 'color') {
      htmlParameters += `&theme=${encodeURIComponent(
        JSON.stringify(JSON.parse(this.state.themeData))
      )}`
      jsParameters += `&theme='+encodeURIComponent(JSON.stringify(themeData)) + '`
      jsVars += `var themeData = ${this.state.themeData}`
    }
    const iframeAttributes = `style="width:100%;height:1000px;" scrolling="no" frameBorder="0"`
    const code = {
      html: `<iframe src="${config.widgetUrl}${externalParameters}${htmlParameters}" ${iframeAttributes}></iframe>`,
      htmlPreview: `<iframe src="${config.widgetPreviewUrl}${previewParameters}${htmlParameters}" ${iframeAttributes}></iframe>`,
      javascript: `
  <script type="application/javascript">
  ${jsVars}
  document.write('<iframe src="${config.widgetUrl}${externalParameters}${jsParameters}" ${iframeAttributes}></iframe>')
  </script>
  `
    }

    const tabWidgetModeToActiveStep = {
      blank: 'integrate',
      color: 'color',
      prefilled: 'report'
    }

    return (
      <div className='App'>
        <AppBar position='static'>
          <Toolbar>
            <Typography variant='title' color='inherit' style={{ flexGrow: 1 }}>
              Medikura Partner Portal Start
            </Typography>
            <Tabs value={tab3} onChange={(e, tab3) => this.setState({ tab3 })}>
              <Tab label='ADR Widget Integration' value='widget' />
            </Tabs>
          </Toolbar>
        </AppBar>
        <AppBar />
        {tab3 === 'widget' && (
          <div style={{ padding: 16, overflowX: 'hidden' }}>
            <Grid container className='App-modeler' spacing={16}>
              <Grid item xs={5}>
                <Paper>
                  <Tabs
                    value={tabWidgetMode}
                    onChange={(e, tabWidgetMode) =>
                      this.setState({
                        tabWidgetMode,
                        activeStep: tabWidgetModeToActiveStep[tabWidgetMode]
                      })
                    }
                  >
                    <Tab
                      label='Quickstart'
                      value='blank'
                      style={{ textTransform: 'inherit' }}
                    />
                    <Tab
                      label='With Custom Layout'
                      value='color'
                      style={{ textTransform: 'inherit' }}
                    />
                  </Tabs>
                  <Stepper orientation='vertical' nonLinear>
                    {(tabWidgetMode === 'color' ||
                      tabWidgetMode === 'prefilled') && (
                      <Step active={activeStep === 'color'}>
                        {' '}
                        <StepButton onClick={_ => this.handleStep('color')}>
                          Configure Layout Information.
                        </StepButton>
                        <StepContent>
                          <Typography style={{ padding: '15px' }}>
                            We use material-ui themes to adjust the look and
                            feel of the widget. Most important is the{' '}
                            <b>palette.primary.main color</b>. See the{' '}
                            <a href='https://v1-4-0.material-ui.com/customization/themes/'>
                              Material UI documentation
                            </a>{' '}
                            for details on the options.
                            <br /> You can change the values below, while
                            checking the preview on the right.
                            <br />
                            When you are done, proceed to the next step and
                            integrate the snippet.
                          </Typography>

                          <AceEditor
                            mode='json'
                            theme='monokai'
                            value={this.state.themeData}
                            onChange={newValue => {
                              if (this.handleValidate(newValue)) {
                                this.setState({ themeData: newValue })
                              }
                            }}
                            name='themeData'
                            style={{ width: '100%', height: '300px' }}
                          />
                        </StepContent>{' '}
                      </Step>
                    )}
                    <Step active={activeStep === 'integrate'}>
                      <StepButton onClick={_ => this.handleStep('integrate')}>
                        Integrate the widget into your website
                      </StepButton>
                      <StepContent>
                        <Typography style={{ padding: '15px' }}>
                          To integrate the widget into your website, just copy
                          the snippet below and paste it into your source code.
                          There are 3 parameters that can be set: <br />
                          <b>sourceDomain</b> - the domain where this widget
                          will be shown, e.g. <code>nebenwirkungen.de</code>.
                          This is used to identify where the reports came from.
                          <br />
                          <b>theme</b> - holding the url-encoded theme
                          definition in a json format. <br />
                          <b>report</b> - holding the url-encoded report
                          definition in a json format. This parameter should
                          somehow be dynamic, depending on your user context.
                        </Typography>

                        <div className='form-inline form-group'>
                          <label htmlFor='sourceDomain' className='mr-2'>
                            Domain:
                          </label>
                          <input
                            type='text'
                            className='form-control flex-fill'
                            value={this.state.sourceDomain}
                            name='sourceDomain'
                            id='sourceDomain'
                            placeholder='Please enter your domain'
                            onChange={e =>
                              this.setState({ sourceDomain: e.target.value })
                            }
                          />
                        </div>

                        <AppBar position='static' color='default'>
                          <Tabs
                            value={tab}
                            onChange={(e, tab) => this.setState({ tab })}
                          >
                            <Tab label='Javascript' value='javascript' />
                            <Tab label='HTML' value='html' />
                          </Tabs>
                        </AppBar>

                        <AceEditor
                          mode='html'
                          theme='monokai'
                          value={code[tab]}
                          name='integrate'
                          style={{
                            height: '300px',
                            width: '100%'
                          }}
                          readOnly
                          wrapEnabled
                        />
                      </StepContent>
                    </Step>
                  </Stepper>
                </Paper>
              </Grid>
              <Grid item xs={7}>
                <Paper>
                  <AppBar position='static' color='default'>
                    <Tabs
                      value={tab2}
                      onChange={(e, tab2) => this.setState({ tab2 })}
                    >
                      <Tab label='Preview' value='preview' />
                    </Tabs>
                  </AppBar>

                  {code && (
                    <>
                      {tab2 === 'preview' && (
                        <div
                          style={{ padding: 25 }}
                          dangerouslySetInnerHTML={{ __html: code.htmlPreview }}
                        />
                      )}
                      {tab2 === 'integration' && (
                        <Paper style={{ margin: 25 }} />
                      )}
                    </>
                  )}
                </Paper>
              </Grid>
            </Grid>
          </div>
        )}
      </div>
    )
  }
}
