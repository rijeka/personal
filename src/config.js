const widgetUrl = 'https://capture.s3.medsvr.net/'
export default {
  widgetUrl: widgetUrl,
  widgetPreviewUrl: widgetUrl
}
