# Setting the base image
FROM node:latest as build

# Create app directory
RUN mkdir -p ~/med-app-partner

# Setting up the working dir
WORKDIR /med-app-partner

# Copying files
COPY . /med-app-partner

# Yarn installation
RUN yarn install

# Bundle app source
COPY . .

# Run the production version
RUN yarn build

# Configuring the nginx
FROM nginx:latest
COPY --from=build /med-app-partner/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

# Initializing nginx
EXPOSE 3000
CMD ["nginx", "-g", "daemon off;"]