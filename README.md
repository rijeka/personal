## Prerequisites

[Node.js](http://nodejs.org/) >= v4 must be installed.

## Installation

-   Running `yarn install` in the app's root directory will install everything you need for development.

## Development Server

-   `yarn start` will run the app's development server at [http://localhost:3000](http://localhost:3000) with hot module reloading.

## Running Tests

-   `yarn test` will run the tests once.

## Building

-   `yarn build` creates a production build by default.
